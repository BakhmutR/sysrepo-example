
#include <unistd.h>
#include <iostream>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>

#include <dbus/dbus.h>
#include <stdbool.h>
#include <unistd.h>

#include "Session.hpp"
#include "testing.pb.h"

#define DBUS_API_SUBJECT_TO_CHANGE


#define MAX_LEN 100

using namespace std;

volatile int exit_application = 0;


/**
 * Connect to the DBUS bus and send a broadcast signal
 */
void sendsignal(const char* sigvalue)
{
   DBusMessage* msg;
   DBusMessageIter args;
   DBusConnection* conn;
   DBusError err;
   int ret;
   dbus_uint32_t serial = 0;

   // initialise the error value
   dbus_error_init(&err);

   // connect to the DBUS system bus, and check for errors
   conn = dbus_bus_get(DBUS_BUS_SESSION, &err);
   if (dbus_error_is_set(&err)) {
      fprintf(stderr, "Connection Error (%s)\n", err.message);
      dbus_error_free(&err);
   }

   if (NULL == conn) {
      exit(1);
   }
   // register our name on the bus, and check for errors
   ret = dbus_bus_request_name(conn, "test.signal.source", DBUS_NAME_FLAG_REPLACE_EXISTING , &err);
   if (dbus_error_is_set(&err)) {
      fprintf(stderr, "Name Error (%s)\n", err.message);
      dbus_error_free(&err);
   }

   if (ret == DBUS_REQUEST_NAME_REPLY_IN_QUEUE ||
       ret == DBUS_REQUEST_NAME_REPLY_EXISTS) {
      exit(1);
   }


   // create a signal & check for errors
   msg = dbus_message_new_signal("/test/signal/Object", // object name of the signal
                                 "test.signal.Type", // interface name of the signal
                                 "Test"); // name of the signal

   if (NULL == msg)
   {
      fprintf(stderr, "Message Null\n");
      exit(1);
   }


   // append arguments onto signal
   dbus_message_iter_init_append(msg, &args);
   if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &sigvalue)) {
      fprintf(stderr, "Out Of Memory!\n");
      exit(1);
   }

   // send the message and flush the connection
   if (!dbus_connection_send(conn, msg, &serial)) {
      fprintf(stderr, "Out Of Memory!\n");
      exit(1);
   }

   dbus_connection_flush(conn);

   printf("Signal to DBus was sent\n");

   // free the message and close the connection

   dbus_message_unref(msg);
   dbus_connection_unref(conn);
}

/* Function to print current configuration state.
 * It does so by loading all the items of a session and printing them out. */
static void
sendCurrentConfig(sysrepo::S_Session session, const char *module_name)
{
    test::info messages;
    char select_xpath[MAX_LEN];
    try {
        snprintf(select_xpath, MAX_LEN, "/%s:*//.", module_name);

        auto values = session->get_items(&select_xpath[0]);
        if(values == nullptr)
        {
            return;
        }
        for(unsigned int i = 0; i < values->val_cnt(); i++)
            messages.add_msg(values->val(i)->to_string());
    } catch( const std::exception& e ) {
        cout << e.what() << endl;
    }

    string serialized;

    if (!messages.SerializeToString(&serialized))
    {
        cout << "Serialization failed";
    }


    sendsignal(serialized.c_str());

}

class My_Callback:public sysrepo::Callback {
    public:

    /* Function to be called for subscribed client of given session whenever configuration changes. */
    int module_change(sysrepo::S_Session sess, const char *module_name, const char*, sr_event_t, \
            uint32_t, void* ) override
    {
        cout << "\n\n ========== CONFIG HAS CHANGED =============\n" << endl;

        sendCurrentConfig(sess, module_name);

        return SR_ERR_OK;
    }
};



static void
sigint_handler(int)
{
    exit_application = 1;
}

/* Notable difference between c implementation is using exception mechanism for open handling unexpected events.
 * Here it is useful because `Conenction`, `Session` and `Subscribe` could throw an exception. */
int
main(int argc, char **argv)
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    const char *module_name = "o-ran-usermgmt";
    try {

        cout << "Application will watch for changes in "<< module_name << endl;

        sysrepo::S_Connection conn(new sysrepo::Connection());
        sysrepo::S_Session sess(new sysrepo::Session(conn));

        sysrepo::S_Subscribe subscribe(new sysrepo::Subscribe(sess));
        sysrepo::S_Callback cb(new My_Callback());

        subscribe->module_change_subscribe(module_name, cb, nullptr, nullptr, 0, SR_SUBSCR_DONE_ONLY);

        sendCurrentConfig(sess, module_name);


        /* loop until ctrl-c is pressed / SIGINT is received */
        signal(SIGINT, sigint_handler);
        while (!exit_application) {
            sleep(1000);  /* or do some more useful work... */
        }

        cout << "Application exit requested, exiting." << endl;

    } catch( const std::exception& e ) {
        cout << e.what() << endl;
        return -1;
    }


    google::protobuf::ShutdownProtobufLibrary();

    return 0;
}




// <users xmlns="urn:o-ran:user-mgmt:1.0">
//        <user>
//          <name>user1</name>
//          <account-type>PASSWORD</account-type>
//          <password>qwerty12345</password>
//          <enabled>true</enabled>
//        </user>
// </users>
