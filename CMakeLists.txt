cmake_minimum_required(VERSION 3.5)

project(test_mav LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include_directories("/usr/local/include/sysrepo-cpp" "/usr/include/dbus-1.0")

add_executable(server main.cpp testing.pb.cc)


add_executable(client client.cpp testing.pb.cc)


target_link_libraries(test_mav sysrepo-cpp protobuf dbus-1)
target_link_libraries(client sysrepo-cpp protobuf dbus-1)
